# Eureka challenge

## Run this command to start the proyect locally, it will build it before it goes up.
```

docker-compose -f development.yml up

```
## Run this to build for production.

```
docker-compose -f production.yml up
```

```
NOTE: If required, use sudo before each command.
```

```
Challenge specs can be found on the following repository.

https://github.com/eurekalabs-io/challenges/tree/main/frontend/vue.js/mums-deals

```